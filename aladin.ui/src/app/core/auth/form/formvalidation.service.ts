import { Injectable } from '@angular/core';
import { LoginData,RegisterData } from '../model';
@Injectable({
  providedIn: 'root'
})
export class FormvalidationService {

  constructor() { }

  validateEmail(email:string):boolean
  {
      var re = /\S+@\S+\.\S+/;
      return re.test(email);
  }


  validatePassword(password:string,pwd:string):boolean{
    if(password!=pwd){
      return false;
    }else{
   return true
    }

  }

  validatePhone(phone:string):boolean{
    if(phone.length<10){
      return false;
    }else{
      return true;
    }

  }
  cleanData(data:RegisterData){
    data.name=data.name.trim().toUpperCase();
    data.email = data.email.trim();
    data.password = data.password.trim();
    data.phone= data.phone.trim();
    return data;

  }






}
