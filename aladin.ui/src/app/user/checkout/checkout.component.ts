import { Component, OnInit,Input,Output,EventEmitter,OnChanges } from '@angular/core';
import { LocalService } from 'src/app/core';
import {  ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements OnInit,OnChanges {
 cart_items:any;
 total:any;
 @Input() data:any;
 city:any;
 address:any;
 @Input() isauth:any;
 @Output() neworder=new EventEmitter<any>();

 tab=["Abidjan","Bassam","Bingerville","Dabou","Jacqueville","Autre"]
 price_zone={
   abjd:0,
   other:0
 }
 isother=false
 delivery_cost:number=0;
 cpt:number=0;
 hasdeliverycost:boolean=false;
 order_data:any;
 payment=false

  constructor(private items:LocalService,private activeroute:ActivatedRoute) { }

  ngOnInit(): void {
    this.cart_items=this.items.cart_items;
     this.total=localStorage.getItem("Total");
     this.total = JSON.parse(this.total);
   // let id=this.activeroute.snapshot.paramMap.get('id');
  
     
  }

  ngOnChanges(){
    this.tab.unshift('Choisir commune..');
    var select = document.getElementById("dlt");
    this.tab.forEach((font) =>{
    var opt = document.createElement('option');
     opt.innerHTML=font;
     if(font==="Abidjan"){
       opt.setAttribute("value","zone1");
     }
     if(font!="Abidjan"){
       opt.setAttribute("value","zone2");
     }
     select?.appendChild(opt);
    })
  }

  ChangeHandler(event:any){
    this.hasdeliverycost=true;
    if(this.cpt==0){
      this.cpt=this.cpt+1;
      if(event.target.value==="zone1"){
        this.city=this.tab[0]
        this.delivery_cost=this.price_zone.abjd
        this.total=+this.total+this.price_zone.abjd
      }else{
        this.total=+this.total+this.price_zone.other;
        this.city=this.tab[+event.target.selectedIndex]
        this.delivery_cost=this.price_zone.other;
        if(this.city=="Autre"){
          this.isother=true
          this.city=""
        }
  
      }
    }
    if(this.cpt>0){
      if(event.target.value==="zone1"){
        this.city=this.tab[0]
        this.total=(+this.total-this.delivery_cost)+this.price_zone.abjd
        this.delivery_cost=this.price_zone.abjd

      }else{
        this.city=this.tab[+event.target.selectedIndex]
        if(this.city=="Autre"){
       this.isother=true
       this.city=""

        }
        this.total=(+this.total-this.delivery_cost)+this.price_zone.other;
        this.delivery_cost=this.price_zone.other;
      
  
      }

    }
    console.log(this.city)
  }
  followp(){
    if(this.address && this.city){
      this.order_data={
        user:this.data,
        delivery:{
          city:this.city,
          address:this.address
        },
        pmode:"MTN MOMO",
        total:this.total
      }
      document.getElementById('checkout')?.setAttribute('hidden',"true")
      this.payment=!this.payment
    }

  }

  backcheck(){
    if(this.payment){
      let check=document.getElementById('checkout')
      if(check)
       check.hidden=!check?.hidden
       this.payment=!this.payment;
    }
  }




}
