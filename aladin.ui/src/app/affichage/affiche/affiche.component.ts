import { Component, OnInit } from '@angular/core';
import { ListService } from 'src/app/core';
@Component({
  selector: 'app-affiche',
  templateUrl: './affiche.component.html',
  styleUrls: ['./affiche.component.scss']
})
export class AfficheComponent implements OnInit {
  disps:any;
  url="/editor/disps/";
  data="Grand format : l'impression grand format est une impression réalisée sur de grands supports tels que: le kakemono, la bâche, le vinyle et le micro-perforé. La qualité de l'impression est meilleure avec des couleurs riches et vibrantes et une superbe reproduction des images."
  constructor( private l :ListService) { }

  ngOnInit(): void {
    this.l.getDisps().subscribe(
      res=>{this.disps=res;},
      err=>{
        console.log(err);
      }
    )
  }

}
