import { Component, OnInit ,EventEmitter,Output,OnChanges} from '@angular/core';
import { ColorEvent } from 'ngx-color';

@Component({
  selector: 'app-text',
  templateUrl: './text.component.html',
  styleUrls: ['./text.component.scss']
})
export class TextComponent implements OnInit,OnChanges {
  hide=false;
  hide1=false;
  hide2=false;
  Text:any;
  textght:any;
  textwdt=40;
  @Output() newTextColorEvent = new EventEmitter<ColorEvent>();
  @Output() newTextFontEvent = new EventEmitter<string>();
  @Output() newTextUnderlineEvent=new EventEmitter<string>();
  @Output() newTextEvent=new EventEmitter<string>();
  @Output() newTextItalicEvent=new EventEmitter<string>();
  @Output() newTextBoldEvent=new EventEmitter<string>();
  @Output() newTextHeightEvent = new EventEmitter<string>();
  @Output() newTextWidthtEvent = new EventEmitter<string>();
  @Output() newTextReturnEvent = new EventEmitter<string>();
  @Output() newTextCopyEvent = new EventEmitter<string>();
  @Output() newTextPasteEvent = new EventEmitter<string>();
  @Output() newTextRemoveEvent=new EventEmitter<string>();
  @Output() newTextOverlineEvent=new EventEmitter<any>();
  @Output() newTextMinusEvent=new EventEmitter<any>();
  @Output() newTextPlusEvent=new EventEmitter<any>();

  colorarray=['#D9E3F0', '#F47373', '#697689', '#37D67A', '#2CCCE4', '#555555',"#D2691E",'#dce775', '#ff8a65', '#ba68c8',"blue","yellow","orange","blacK","red","indigo","green","brown","#800080","#808000","#000080"];
  constructor() { }

  ngOnInit(): void {
    
  }
ngOnChanges(){

}

 cache(){
  this.hide=!this.hide
  this.hide1=false
 }
cache1(){
  this.hide1=!this.hide1
  this.hide=false
}
cache2(){
  this.hide2=!this.hide2
  this.hide1=false
  this.hide=false
}

textoverline(event:any){
  this.newTextOverlineEvent.emit(event);
}

selectChangeHandler(event:any) {
  this.newTextFontEvent.emit(event)
  
}


texteclor($event:ColorEvent){
  this.newTextColorEvent.emit($event)

}

textintalic(event:any){
  this.newTextItalicEvent.emit(event)
}

textbold(event:any){
  this.newTextBoldEvent.emit(event);
}


textunderline(){
  this.newTextUnderlineEvent.emit();
}

textheight(event:any){
  this.newTextHeightEvent.emit(event);
}

textwidth(event:any){
  this.newTextWidthtEvent.emit(event);
}
  
textreturn(){
  this.newTextReturnEvent.emit();
}


copy(){
this.newTextCopyEvent.emit();
}


paste(){
this.newTextPasteEvent.emit();
}

text(event:any){
  this.newTextEvent.emit(event);
}

textremove(){
  this.newTextRemoveEvent.emit(); 

}

minus(event:any){
  this.textwdt=+this.textwdt - 1;

  this.newTextMinusEvent.emit(+event-1);

}

plus(event:any){
  this.textwdt=+this.textwdt + 1;
  this.newTextPlusEvent.emit(+event+1);
  
} 


}
