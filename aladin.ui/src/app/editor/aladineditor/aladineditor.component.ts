import { Component, OnInit ,OnChanges,ViewChild} from '@angular/core';
import { fabric } from 'fabric';
import { install } from 'chart-js-fabric';
install(fabric);
import { ActivatedRoute } from '@angular/router';
import {TextfontService,ListService,LocalService ,LoginService} from '../../core';
declare var require: any
var $ = require("jquery");
import { jsPDF } from "jspdf";

var FontFaceObserver = require('fontfaceobserver');
import { ColorEvent } from 'ngx-color';
var myalert=require('sweetalert2')

import { ContextMenuComponent } from 'ngx-contextmenu';

  @Component({
    selector: 'app-aladineditor',
    templateUrl: './aladineditor.component.html',
    styleUrls: ['./aladineditor.component.scss']
  })
  export class AladineditorComponent implements OnInit ,OnChanges{
    canvas:any;
    s:any;
    face1:any;
    dt:any=[];
    selectedDay="blue";
    isbold:boolean =false;
    Text:any;
    url:any;
    stroke_size=1;
    strokecolor="#0B44A5"
    Textcolor="#0B44A5";
    stroke_color="blue"
    colorarray=['#D9E3F0', '#F47373', '#697689', '#37D67A', '#2CCCE4', '#555555',"#D2691E",'#dce775', '#ff8a65', '#ba68c8',"blue","yellow","orange","blacK","red","indigo","green","brown","#800080","#808000","#000080"];
    name:any;
    text_height:any;
    text_width:any;
    imgsrc:any;
    selecetdFile:any;
    public fontFace: any;
    imagePreview:any;
    fonts = ["Choix de police","Arial","roboto",'Tangerine','caveat',"poppins","Dancing Script","cinzel","Festive","satisfy","Yellowtail","pacifico","monoton","Oleo Script","Pinyon Script"];
    designed:any=[];
    para:any;
    logo:any;
    prodid:any;
    iscat:any;
    Image:any;
    show:any=false;
    active:boolean=true;
    pback:any;
    pfront:any;
    colors:any=["indigo"];
    objectToSendBack:any;
    face2:any;
    origin:any;
    cactive:boolean=false;
    front_data:any=[];
    back_data:any=[];
    cart_length:any;
    textmenu=false;
    current_col:any;
    production_f1:any;
    production_f2:any;
    @ViewChild(ContextMenuComponent) public basicMenu:any= ContextMenuComponent;

    constructor(private Url:TextfontService,private route: ActivatedRoute,private p:ListService,private L:LocalService,private auth:LoginService) { 
    }

   ngOnChanges(){
     this.cart_length=this.L.cart_items
   }

   Show(){
  this.textmenu=true;
  }


  hide(){
    this.textmenu=false;

  }
texteclor($event:ColorEvent){
     let color = $event.color.hex;   
      let item= this.canvas.getActiveObject();

      if(item && item.type!="activeSelection"){
        item.set({fill:color});
        this.canvas.renderAll(item);
      }

      if(item && item.type=="activeSelection"){
        for(let el of item._objects){
          el.set({fill:color});
          this.canvas.renderAll(el);
        }
       }
    this.canvas.requestRenderAll();
}


setStroke($event:ColorEvent){
  let color=$event.color.hex;
  let item =  this.canvas.getActiveObject();
  if(item && item.type!="activeSelection"){
    item.set({strokeWidth:this.stroke_size,stroke:color});
    this.canvas.renderAll(item);
  }
  if(item && item.type=="activeSelection"){
    for(let el of item._objects){
      el.set({strokeWidth:this.stroke_size,stroke:color});
      this.canvas.renderAll(el);
    }
  }
    this.canvas.requestRenderAll();
}


setshdow($event:ColorEvent){
  let color=$event.color.hex;
  let item =  this.canvas.getActiveObject();
  if(item && item.type!="activeSelection"){
    item.set({shadow:color +"5px 5px 5px"});
    this.canvas.renderAll(item);
  }
  if(item && item.type=="activeSelection"){
    for(let el of item._objects){
      el.set({shadow:color+"5px 5px 5px"});
      this.canvas.renderAll(el);
    }
  }
    this.canvas.requestRenderAll();
}

changeComplete($event:ColorEvent){
      let image;
      console.log(this.dt[0])
      for(let elt of this.dt){
          if($event.color.hex==elt.lib){
            image=elt.lib_img;
            this.s=image;
            this.face1=elt.lib_back;
            this.current_col=$event.color.hex;
            break
            }
  }
       if(image!=undefined){
        this.canvas.backgroundImage=null;
        this.makeImage(image);
        this.cactive=true;
        this.origin=null;
        this.face2=null;

      }

    }


    toggle(){
      this.show=!this.show;
    }


    OnRightClick(event:any){   
      let item = this.canvas.getActiveObject();
      if(item){
        var elt=document.getElementById('xdv')
        this.p.triggerMouse(elt)   
      }  
      return false
    }

    ngOnInit(): void { 
      this.cart_length=this.L.cart_items;
   // localStorage.removeItem("designs");
  
    this.canvas = new fabric.Canvas('aladin-editor',{ 
      hoverCursor: 'pointer',
      selection: true,
      selectionBorderColor:'blue',
      fireRightClick: true,
      preserveObjectStacking: true,
      stateful:true
    }); 
  
    this.canvas.filterBackend=new fabric.WebglFilterBackend();
    this.canvas.on('mouse:move', (event:any)=>{
      this.onMouseMove(event);
    });
    this.canvas.on('mouse:down', (event:any)=>{
      this.onMouseDown();   

    });

    this.canvas.on('mouse:out', (event:any)=>{

      this.hide();   

    });
    this.canvas.on("selection:created", (event:any)=>{
      this.objectToSendBack = event.target;
      this.sendSelectedObjectBack()
    }); 
    this.canvas.on("selection:updated",(event:any)=>{
      this.objectToSendBack = event.target;
    });


  this.name=this.route.snapshot.paramMap.get('id');
  this.para =this.route.snapshot.paramMap.get('name');
  let text_h= document.getElementById("Range3")

  text_h?.addEventListener('input',()=>{
    let text = this.canvas.getActiveObject();
    if(text && text.type=="activeSelection"){
      for(let el of text._objects){
        el.set({fontSize:this.text_height,height:this.text_height});
        this.canvas.renderAll(el)

      }

    }
    if(text && text.type!="activeSelection"){
      text.set({fontSize:this.text_height,height:this.text_height});
      this.canvas.renderAll(text)
    }
    this.canvas.requestRenderAll();

  });
  
  $("body").children().first().before($(".modal"));

  
  document.getElementById('strokesize')?.addEventListener('input',()=>{
    if(this.canvas.getActiveObject()!=undefined && this.canvas.getActiveObject().type=="activeSelection"){
      for(let el of this.canvas.getActiveObject()._objects){
        el.set({strokeWidth:this.stroke_size,stroke:this.stroke_color});
        this.canvas.renderAll(el);
      }
    }
    if(this.canvas.getActiveObject()!=undefined && this.canvas.getActiveObject().type!="activeSelection"){
      this.canvas.getActiveObject().set({strokeWidth:this.stroke_size,stroke:this.stroke_color});
      this.canvas.renderAll(this.canvas.getActiveObject());
    }
    this.canvas.requestRenderAll();
  });
 

   if(this.para=="cloth"){
    this.iscat=this.para;
    this.prodid=this.name;
    this.toggle();
    this.p.getcloth(this.name).subscribe(res=>{
      this.dt=res;  
      console.log(this.dt)
      this.toggle();
      for(let elt of this.dt){
       this.s=elt.front_side;
       this.face1=elt.back_side;
       if(this.colors.indexOf(elt.lib)==-1){
        this.colors.push(elt.lib);

       }
       if(+elt.is_back==0){
        this.imgsrc=elt.front_side;
        this.Text=JSON.parse(elt.data);
               
       }
       
      }
      this.makeText(this.Text.fill,this.Text.fontFamily,this.Text.text,this.Text.top,this.Text.left,this.Text.height,this.Text.width,this.Text.stroke,this.Text.strokeWidth,this.Text.fontSize)

      this.makeImage(this.imgsrc);
    },
    err=>{console.log(err)}
    );
   }

   if(this.para=="gadgets"){
    this.iscat=this.para;
    this.prodid=this.name;
    this.p.getGadget(this.name).subscribe(
      res=>{
      this.dt=res;
      for(let elt of this.dt){
       this.s=elt.img;
       this.face1=elt.back_side;
       if(this.colors.indexOf(elt.lib)==-1){
        this.colors.push(elt.lib);

       }
        if(+elt.is_back==0){
        this.imgsrc=elt.front_side;
        this.Text=JSON.parse(elt.data);
        this.makeText(this.Text.fill,this.Text.fontFamily,this.Text.text,this.Text.top,this.Text.left,this.Text.height,this.Text.width,this.Text.stroke,this.Text.strokeWidth,this.Text.fontSize)
     
       }
      
      }
      this.makeImage(this.imgsrc);

    },
    err=>{
      this.toggle();
      console.log(err);
    }
    );
   }

   let ofont:any=[]
   this.p.getfonts().subscribe(
     res=>{
       ofont=res
       if(ofont.status==200){
         for(let item of ofont.fonts){
           this.fonts.push(item.name);
           this.p.addStylesheetURL(item.url);
         }
         this.fonts.unshift('Times New Roman');
         var select = document.getElementById("family");
         this.fonts.forEach(function(font) {
         var opt = document.createElement('option');
          opt.innerHTML=font;
          opt.value=font;
          select?.appendChild(opt);
            });
            var select = document.getElementById("font-family");
            this.fonts.forEach(function(font) {
            var opt = document.createElement('option');
             opt.innerHTML=font;
             opt.value=font;
             select?.appendChild(opt);
            });
       }
     },
     err=>{
       console.log(err);
     }
   )
   
   if(this.para=="disps"){
    this.iscat=this.para;
    this.prodid=this.name;
    this.p.getDisp(this.name).subscribe(res=>{
      this.dt=res;
      for(let elt of this.dt){
       this.s=elt.img;
       if(this.colors.indexOf(elt.lib)==-1){
        this.colors.push(elt.lib);

       }
       this.face1=elt.back_side;
       if(+elt.is_back==0){
        this.imgsrc=elt.front_side;
        this.Text=JSON.parse(elt.data);
        this.makeText(this.Text.fill,this.Text.fontFamily,this.Text.text,this.Text.top,this.Text.left,this.Text.height,this.Text.width,this.Text.stroke,this.Text.strokeWidth,this.Text.fontSize)
      
      }
      
      }
      this.makeImage(this.imgsrc);

    },
    err=>{console.log(err)}
    );
   }

   if(this.para=="prints"){
    this.iscat=this.para;
    this.prodid=this.name;
    this.p.getprint(this.name).subscribe(res=>{
      this.dt=res;
      for(let elt of this.dt){
       this.s=elt.img;
       if(this.colors.indexOf(elt.lib)==-1){
        this.colors.push(elt.lib);
       }
       this.face1=elt.back_side
       if(+elt.is_back==0){
        this.imgsrc=elt.front_side;
        this.Text=JSON.parse(elt.data);
        this.makeText(this.Text.fill,this.Text.fontFamily,this.Text.text,this.Text.top,this.Text.left,this.Text.height,this.Text.width,this.Text.stroke,this.Text.strokeWidth,this.Text.fontSize)
      }
       
      }
      this.makeImage(this.imgsrc);

    },
    err=>{console.log(err)}
    );
   }

   if(this.para=="packs"){
    this.iscat=this.para;
    this.prodid=this.name;
    this.p.getPack(this.name).subscribe(res=>{
      this.dt=res
      for(let elt of this.dt){
       this.s=elt.front_side;
       this.face1=elt.back_side;
       if(this.colors.indexOf(elt.lib)==-1){
        this.colors.push(elt.lib);

       }       
       if(+elt.is_back==0){
        this.imgsrc=elt.front_side;
        this.makeImage(this.imgsrc);

      }
       
      }
      this.makeImage(this.imgsrc)
    },
    err=>{console.log(err)}
    );
   }
   if(this.para=="tops"){
    this.iscat=this.para;
    this.prodid=this.name;
    this.p.gettop(this.name).subscribe(res=>{
      this.dt=res;
      for(let elt of this.dt){
       this.imgsrc=elt.front_side;
       this.s=elt.img;
       this.makeImage(this.imgsrc);


      }
    },
    err=>{console.log(err)}
    );
}

}

underline(){
  let item=this.canvas.getActiveObject()
  if(item && item.type!="activeSelection"){
    if(!item.underline){
      item.set({underline:true});
      this.canvas.renderAll(item);

    }else{
      item.set({underline:false});
      this.canvas.renderAll(item);
  
    }
  }

  if(item && item.type=="activeSelection"){

    for(let el of item._objects){
      if(!item.underline){
        el.set({underline:true});
        this.canvas.renderAll(el);

      }else{
        el.set({underline:false});
        this.canvas.renderAll(el);
      }
    }
  }
  this.canvas.requestRenderAll();

}

turnImage(event:any){
  let e=event.target.id;
  this.process_back(e);
  this.process_front(e);
}


process_back(id:string){
  let src;
  console.log(id);

  if(id==="imgback" && !this.active){
    let objts=this.canvas.getObjects();
    if(objts.length>0){
      for(let i=0;i<objts.length;i++){
        this.canvas.remove(objts[i]);
      }
     // this.canvas.renderAll();
    }
    if(this.back_data.length>0){  
      for(let elt of this.back_data){
        this.canvas.add(elt);
        this.canvas.renderAll(elt);
      }
        for(let elt of this.dt){
          if(this.cactive){
            if(+elt.is_back==1 && this.current_col==elt.lib){
              src=elt.lib_back;
              break;
            }

          }else{
            if(+elt.is_back==1){
              src=elt.back_side;
              console.log(src);

            break;
          
            }
            

          }
          
        
    }
    this.makeImage(src);


    }else{
      for(let elt of this.dt){
        if(this.cactive){
          if(+elt.is_back==1 && this.current_col==elt.lib){
            src=elt.lib_back;
            this.Text=JSON.parse(elt.data);
            break;
          }
      
        }else{
          if(+elt.is_back==1){
            src=elt.back_side;
            this.Text=JSON.parse(elt.data);
            console.log(this.active);
            console.log(src);
            break;
          }
       

        }
        
      }
      this.makeText(this.Text.fill,this.Text.fontFamily,this.Text.text,this.Text.top,this.Text.left,this.Text.height,this.Text.width,this.Text.stroke,this.Text.strokeWidth,this.Text.fontSize);     
      this.makeImage(src);
    }
  }

}
  

process_front(e:string){
  console.log(e);
  let src;
  if(e==="imgfront" && this.active){
    let objts=this.canvas.getObjects()
    if(objts.length>0){
      for(let i=0;i<objts.length;i++){
        this.canvas.remove(objts[i]);
      }
     this.canvas.backgroundImage=null;

    }
    if(this.front_data.length>0){
  
      for(let elt of this.front_data){
        this.canvas.add(elt);
        this.canvas.renderAll(elt);
      }
        for(let elt of this.dt){
          if(this.cactive){
            if(+elt.is_back==0 && this.current_col==elt.lib){
              src=elt.lib_img;
              break;
            }

          }else{
            if(+elt.is_back==0){
              src=elt.img;
              this.Text=JSON.parse(elt.data);
              console.log(this.active);
              break;
            }

          }
  }
    this.makeImage(src);
    }else{
      for(let elt of this.dt){
        if(this.cactive){
          if(+elt.is_back==0 && this.current_col==elt.lib){
            src=elt.lib_img;
            this.Text=JSON.parse(elt.data);
            break
          }
      
        }else{
          if(+elt.is_back==0){
            src=elt.img;
            this.Text=JSON.parse(elt.data);
             break;
          }
         

        }
        
      }
      this.makeText(this.Text.fill,this.Text.fontFamily,this.Text.text,this.Text.top,this.Text.left,this.Text.height,this.Text.width,this.Text.stroke,this.Text.strokeWidth,this.Text.fontSize);     
      this.makeImage(src);
    }
  }

}




textwidthminus(event:any){
  let item = this.canvas.getActiveObject()
  item.set({width:+event,fontSize:+event});
   this.canvas.renderAll(item);
   this.canvas.requestRenderAll();
}
textwidthplusOne(event:any){
  let item = this.canvas.getActiveObject()
  item.set({width:+event,fontSize:+event});
   this.canvas.renderAll(item);
   this.canvas.requestRenderAll();
}

textwidth(event:any){
  let text_w =document.getElementById("R")
  let item = this.canvas.getActiveObject()
       item.set({width:+event,fontSize:+event});
        this.canvas.renderAll(item);
        this.canvas.requestRenderAll();

  text_w?.addEventListener("input",()=>{
    if(item){
      if(item.type!="activeSelection"){
        item.set({width:+event,fontSize:+event});
        this.canvas.renderAll(item);
        this.canvas.requestRenderAll();
      }
    }
    console.log(event);
});
}

selectChangeHandler(event: any) {
      let item= this.canvas.getActiveObject();
      this.Text=item;
      this.loadfont(event.target.value);
      if(this.Text && this.Text.type=="activeSelection"){
        for(let el of this.Text._objects){
          el.set({
            fontFamily:event.target.value
          });
          this.canvas.renderAll(el);
          this.canvas.requestRenderAll();

        }

      }

      if(this.Text && this.Text.type!="activeSelection"){
        this.Text.set({
          fontFamily:event.target.value
        });
        this.canvas.renderAll(this.Text);
        this.canvas.requestRenderAll();
      }
     

    }

    setTextBeforeEdit(){
      let text = this.canvas.getActiveObject()
      if(text && text.type!="activeSelection"){ 
        if(text.underline && text._textBeforeEdit){
          text.set({text:text._textBeforeEdit,underline:false});
          this.canvas.renderAll(text);

        }   
        if(text._textBeforeEdit){
          text.set({text:text._textBeforeEdit});
          this.canvas.renderAll(text);

        }
      } 
  
      if(text && text.type=="activeSelection"){
        for(let el of text._objects){
          if(el.underline && el._textBeforeEdit){
            el.set({text:el._textBeforeEdit,underline:false});
            this.canvas.renderAll(el);

          }   
          if(el._textBeforeEdit){
            el.set({text:el._textBeforeEdit});
            this.canvas.renderAll(el);
          }

        }
      }
      this.canvas.requestRenderAll();

    }

    removeItem(){
      let item = this.canvas.getActiveObject();
      if(item && item.type!="activeSelection"){
        this.canvas.remove(item);
      }

      if(item && item.type=="activeSelection"){
        for(let e of item._objects){
          this.canvas.remove(e);
        }
      }

    }
    

    MakeItalic(event:any){
      let item= this.canvas.getActiveObject();
      console.log(this.canvas);
      this.Text=item;
      if(this.Text!=undefined && this.Text.type=="activeSelection"){
        for(let el of this.Text._objects){
          if(el.fontStyle!="normal"){
            el.set({fontStyle:'normal'});
            this.canvas.renderAll(el)
           
          }else{
            el.set({fontStyle:'italic'});
            this.canvas.renderAll(el);
          }
        }
        
      }
      if(this.Text && this.Text!="activeSelection"){
        if(this.Text.fontStyle!="normal"){
          this.Text.set({fontStyle:"normal"});
          this.canvas.renderAll(this.Text);
        }else{
          this.Text.set({
            fontStyle:"italic"
          });
          this.canvas.renderAll(this.Text);
        }
      }
      this.canvas.requestRenderAll();

    }

    MakeBold(event:any){
      let item= this.canvas.getActiveObject();
      this.Text=item;  
      if(this.Text!=undefined && this.Text.type=="activeSelection"){    
          for(let el of this.Text._objects){
            if(el.fontWeight=="normal"){
            el.set({fontWeight:'bold'});
            this.canvas.renderAll(el);
          }else{
              el.set({fontWeight:'normal'});
              this.canvas.renderAll(el);        
          }
        }
        this.canvas.requestRenderAll();  
      }
        
      if(this.Text!=undefined && this.Text.type!="activeSelection"){
        if(this.Text.fontWeight=="normal"){   
          this.Text.set({fontWeight:'bold'});
            this.canvas.renderAll(this.Text);
            this.canvas.requestRenderAll();
         
        }else{
            this.Text.set({fontWeight:'normal'});
            this.canvas.renderAll(this.Text);
            this.canvas.requestRenderAll();   
        }

      }
    }

   sendSelectedObjectBack =()=>{
      this.canvas.sendToBack(this.objectToSendBack);
    }


    changeTextColor(event:any){
      this.selectedDay = event.target.value;
      let item= this.canvas.getActiveObject();
      if(item.type=="activeSelection"){
        console.log(item._objects);
        for(let el of item._objects){
          el.set({fill:this.selectedDay});
          this.canvas.renderAll(el);
        }
      }
      else{
        item.set({fill:this.selectedDay});
        this.canvas.renderAll(item);
      } 
      this.canvas.requestRenderAll();
    }


    setImage(event:any){
      this.colors=["indigo"];
      this.back_data=[];
      this.front_data=[];
      let id=event.target.id;
      let m=event.target.name;
      var objt=this.canvas.getObjects()
      for(let i=0;i<objt.length;i++){
        this.canvas.remove(objt[i]);
      }
      this.canvas.backgroundImage=null;
      this.origin=null;
      this.face2=null;
      this.cactive=false;
      this.active=true;
      if(id!=undefined && m!=undefined){
        //vetements 
     
      // this.cactive=false;
        if(m=="cloth"){
          this.iscat=m;
           this.prodid=id;
           this.p.getcloth(id).subscribe(res=>{
          this.dt=res;   
          for(let elt of this.dt){
           this.s=elt.front_side;
           this.face1=elt.back_side;
            if(this.colors.indexOf(elt.lib)==-1){
            this.colors.push(elt.lib);         
          }
         if(+elt.is_back==0){
          this.imgsrc=elt.front_side;
          this.Text=JSON.parse(elt.data);
      }
       
      }
      this.makeImage(this.imgsrc);
      this.makeText(this.Text.fill,this.Text.fontFamily,this.Text.text,this.Text.top,this.Text.left,this.Text.height,this.Text.width,this.Text.stroke,this.Text.strokeWidth,this.Text.fontSize);
      this.p.triggerMouse(document.getElementById('closept')); 

    },
   err=>{console.log(err);}
    );

    }
     //gadgets
      if(m=="gadgets"){
        this.iscat=m;
        this.prodid=id;
        this.p.getGadget(id).subscribe(res=>{
          this.dt=res;
          for(let elt of this.dt){
           this.s=elt.front_side;
           this.face1=elt.back_side;
           if(this.colors.indexOf(elt.lib)==-1){
            this.colors.push(elt.lib);
        }
           if(+elt.is_back==0){
            this.imgsrc=elt.front_side;
            this.Text=JSON.parse(elt.data);
            this.makeText(this.Text.fill,this.Text.fontFamily,this.Text.text,this.Text.top,this.Text.left,this.Text.height,this.Text.width,this.Text.stroke,this.Text.strokeWidth,this.Text.fontSize)    
            break

          }
          
          }
          this.makeImage(this.imgsrc);

        var elt=document.getElementById('closept');
        this.p.triggerMouse(elt); 
        },
        err=>{console.log(err)}
        );
       }
    
       //disps
       if(m=="disps"){
        this.iscat=m;
        this.prodid=id;
    
        this.p.getDisp(id).subscribe(res=>{
          this.dt=res;
          for(let elt of this.dt){
           this.s=elt.front_side;
           this.face1=elt.back_side;
           if(this.colors.indexOf(elt.lib)==-1){
            this.colors.push(elt.lib);
    
           }    
           if(+elt.is_back==0){
            this.imgsrc=elt.front_side;
            this.Text=JSON.parse(elt.data);
            this.makeText(this.Text.fill,this.Text.fontFamily,this.Text.text,this.Text.top,this.Text.left,this.Text.height,this.Text.width,this.Text.stroke,this.Text.strokeWidth,this.Text.fontSize)               }
           
          }
          this.makeImage(this.imgsrc)
          var elt=document.getElementById('closept');
        this.p.triggerMouse(elt); 
        },
        err=>{console.log(err);}
        );
       }
       //packs
       if(m=="packs"){
        this.iscat=m;
        this.prodid=id;
        this.p.getPack(id).subscribe(res=>{
          this.dt=res;
          for(let elt of this.dt){
           this.s=elt.front_side;
           this.face1=elt.back_side;
           if(this.colors.indexOf(elt.lib)==-1){
            this.colors.push(elt.lib);
          }
           if(+elt.is_back==0){
            this.imgsrc=elt.front_side;
            this.Text=JSON.parse(elt.data);
            this.makeText(this.Text.fill,this.Text.fontFamily,this.Text.text,this.Text.top,this.Text.left,this.Text.height,this.Text.width,this.Text.stroke,this.Text.strokeWidth,this.Text.fontSize)    
          }
           
          }
          this.makeImage(this.imgsrc);
          var elt=document.getElementById('closept');
        this.p.triggerMouse(elt); 
        },
        err=>{console.log(err)}
        );
       }
       //printed
       if(m=="prints"){
        this.iscat=m;
        this.prodid=id;
        this.p.getprint(id).subscribe(res=>{
          this.dt=res;
          for(let elt of this.dt){
           this.s=elt.front_side;
           this.face1=elt.back_side;
           if(this.colors.indexOf(elt.lib)==-1){
            this.colors.push(elt.lib);
    
           }
    
           if(+elt.is_back==0){
            this.imgsrc=elt.front_side;
            this.Text=JSON.parse(elt.data);
            this.makeText(this.Text.fill,this.Text.fontFamily,this.Text.text,this.Text.top,this.Text.left,this.Text.height,this.Text.width,this.Text.stroke,this.Text.strokeWidth,this.Text.fontSize);    
           }
          }
          this.makeImage(this.imgsrc)
          var elt=document.getElementById('closept');
        this.p.triggerMouse(elt); 
        },
        err=>{console.log(err);}
        );
       }
      }
    }

  makeImage(src:any):any{
      var img = new Image();
      let canvas= this.canvas;
      img.src=src
      img.crossOrigin ='anonymous';
      this.Image=img;        
      img.onload = function() {
      var f_img = new fabric.Image(img);
   
      canvas.setBackgroundImage(f_img,canvas.renderAll.bind(canvas), {
              backgroundImageOpacity: 0.1,
              backgroundImageStretch: true,
              left:5,
              top:5,
              right:5,
              bottom:5,
              height:600,
              width:600,

          });
          canvas.centerObject(f_img);
          canvas.renderAll(f_img);
          canvas.requestRenderAll();     
 }

  }

createText(event:any){
  this.Text= new fabric.IText("TEXTE",{
    top:200,
    left:200,
    fill:"blue",
    fontSize:38,
    fontStyle:'normal',
    cornerStyle:'circle',
    selectable:true,
    borderScaleFactor:1,
    overline:false
  });
  var r= new fabric.Rect({
    width: 200, 
    height: 100, 
    fill: '#FFFFFF',
  })
  var group = new fabric.Group([ ], {
    left: 100,
    top: 100,
    width:300,
    height:400,
    lockScalingX: false,
    lockScalingY: false,
    hasRotatingPoint: true,
    transparentCorners: true,
    cornerSize: 8,
    selectable:true,
   
  });
 
  this.canvas.add(this.Text).setActiveObject(this.Text);
  this.canvas.renderAll(this.Text);
  this.canvas.requestRenderAll()
  this.canvas.centerObject(this.Text)
 
 
}

textoverline(event:any){
    let item = this.canvas.getActiveObject()
    if(item.type=="activeSelection"){
      for(let i of item){
        if(!i.overline){
          i.set({overline:true})
          this.canvas.renderAll(i);
          this.canvas.requestRenderAll()
        }else{
          i.set({overline:false});
          this.canvas.renderAll(i);
          this.canvas.requestRenderAll()
        }
      }
    }
    
    if(item.type!="activeSelection"){
        if(!item.overline){
          item.set({overline:true});
          this.canvas.renderAll(item);
          this.canvas.requestRenderAll()
        }else{
          item.set({overline:false});
          this.canvas.renderAll(item);
          this.canvas.requestRenderAll()
        }
      
    }
}

makenewText(fill:any,font:any,text:string,top:number,left:number,height:number,width:number,strok:string,strokwidth:number){
  this.Text= new fabric.IText(text,{
    top:top,
    left:left,
    fill:fill,
    fontSize:25,
    fontStyle:'normal',
    cornerStyle:'circle',
    fontFamily:font,

  });
  this.Text.set({width:width,height:height,stroke:strok,strokWidth:strokwidth})
  this.canvas.add(this.Text).setActiveObject(this.Text);
  this.canvas.centerObject(this.Text)
  this.canvas.renderAll(this.Text);
  this.canvas.requestRenderAll();


}

  makeText(fill:any,font:any,text:string,top:number,left:number,height:number,width:number,strok:string,strokwidth:number,fontsize:number){
      this.Text= new fabric.IText(text,{
        top:top,
        left:left,
        fill:fill,
        fontSize:fontsize,
        fontStyle:'normal',
        cornerStyle:'circle',
        fontFamily:font,
      });
      console.log(height,width,strok);
      this.Text.set({width:width,height:height,stroke:strok,strokWidth:strokwidth})
      this.canvas.add(this.Text).setActiveObject(this.Text);
      this.canvas.renderAll(this.Text);
      this.canvas.requestRenderAll();
  
  }


setLogo(event:any){
  event.target.setAttribute("crossOrigin",'anonymous');
var imgInstance = new fabric.Image(event.target, {
  left: 100,
  top: 100,
  width:350,
  height:350
});

this.canvas.add(imgInstance).setActiveObject(imgInstance);
this.canvas.centerObject(imgInstance);
this.canvas.requestRenderAll();
this.logo=imgInstance;
}

onFileUpload(event:any){
this.selecetdFile = event.target.files[0];
 const reader = new FileReader();
 reader.onload = () => {
  this.imagePreview = reader.result;
  };
  reader.readAsDataURL(this.selecetdFile);
   
  }


loadfont(f:any){
    let fonts=new FontFaceObserver(f);
    fonts.load().then(function () {

    }).catch((err:any)=>{
      console.log(err);
    });
  }

Copy() {
  let canvas=this.canvas;
	canvas.getActiveObject().clone(function(cloned:any) {
		canvas._clipboard= cloned;
	});
}


Paste() {
  let canvas=this.canvas
	// clone again, so you can do multiple copies.
	canvas._clipboard.clone(function(clonedObj:any) {
		canvas.discardActiveObject();
		clonedObj.set({
			left: clonedObj.left + 15,
			top: clonedObj.top + 15,
			evented: true,
		});
		if (clonedObj.type === 'activeSelection') {
			// active selection needs a reference to the canvas.
			clonedObj.canvas = canvas;
			clonedObj.forEachObject(function(obj:any) {
				canvas.add(obj);
			});
			// this should solve the unselectability
			clonedObj.setCoords();
		} else {
			canvas.add(clonedObj);
		}
		canvas._clipboard.top += 15;
		canvas._clipboard.left += 15;
		canvas.setActiveObject(clonedObj);
		canvas.requestRenderAll();
	});
}
savedesign(event:any){

  if(this.active){
    let items = this.canvas.getObjects();
    for(let el of items){
      this.front_data.push(el);
    }
      this.origin=this.canvas.toDataURL("png");
      this.s=this.origin;
      this.active=!this.active;
      this.canvas.backgroundImage=null;
      this.production_f1=this.canvas.toDataURL()
     
      var elt=document.getElementById('imgback');
      this.p.triggerMouse(elt);  
    

  }else{
    this.face2=this.canvas.toDataURL("png");
    this.face1=this.face2;
    let items = this.canvas.getObjects();
    for(let el of items){
      this.back_data.push(el);
    }
    this.canvas.backgroundImage=null;
    this.production_f2=this.canvas.toDataURL()
    this.active=!this.active;
    var elt=document.getElementById('imgfront');
    this.p.triggerMouse(elt);
  
 }
 
if(this.origin && this.face2){
 let dataset={}; 
 let prod_info={};
   for(let item of this.dt){
     Object.assign(prod_info,{
      name:item.name_cloths,
      price:item.price,
      size:JSON.parse(item.size),
      type:JSON.parse(item.type_imp),
      material:item.material,
      lib_img:item.lib_img,
      lib_back:item.lib_back,
      front:item.front_side,
      back:item.back_side,
      user_id:item.user_id,
      staff:item.staff,
      id:item.clt_id
      
     })
   break;
 }
 Object.assign(dataset,{face1:this.origin,face2:this.face2,text1:this.front_data,text2:this.back_data,prod:prod_info,production_f1:this.production_f1,production_f2:this.production_f2});
 try{
   if(this.L.items.length>8){
    myalert.fire({
      title:"espace de stockage local",
      icon: 'info',
      html:"<p style='color:red,fontSize:small'>Vous ne pouvez sauvegader que 8 designs au maximun dans l'espace local veuillez ajouter vos designs au panier afin de poursuivre vos opérations.</p>"
    }
    )
   }else{
    this.L.add(dataset);
   
   }
 }catch(err:any){
   if(err.code==22){
    myalert.fire({
      title:"espace de stockage local",
      icon: 'info',
      html:"<p style='color:red,fontSize:small'>Vous ne pouvez puls enregistrer de designs dans l'espace local, veuillez supprimer certains de vos designs afin de pouvoir pousuvre vos operations sur aladin.ci </p>"
    }
    )
      console.log(err);
   }else if(err=="QUOTA_EXCEEDED_ERR"){
    myalert.fire({
      title:"espace de stockage local",
      icon: 'info',
      html:"<p style='color:red,fontSize:small'>Vous ne pouvez puls enregistrer de designs dans l'espace local, veuillez supprimer certains de vos designs afin de pouvoir pousuvre vos operations sur aladin.ci </p>"
    }
    )
   }
 
 }


}




}

onMouseDown=()=>{
 let canvas= this.canvas;
 let key=0
$(document).keydown((e:any)=>{
  if( e.which === 89 && e.ctrlKey){
     console.log(e.which,e.ctrlKey);
  }
  else if(e.which === 90 && e.ctrlKey){
     console.log(e.which,e.ctrlKey)
  } else if(e.which === 67 && e.ctrlKey){
    console.log(e.which,e.ctrlKey)
    canvas.getActiveObject().clone(function(cloned:any) {
      canvas._clipboard= cloned;
    });

  }

  else if(e.which === 86 && e.ctrlKey){
    console.log(e.which,e.ctrlKey);
    if(e.ctrlKey)
    {
      canvas._clipboard.clone(function(clonedObj:any) {
        canvas.discardActiveObject();
        clonedObj.set({
          left: clonedObj.left + 15,
          top: clonedObj.top + 15,
          evented: true,
        });
        if (clonedObj.type === 'activeSelection') {
          // active selection needs a reference to the canvas.
          clonedObj.canvas = canvas;
          clonedObj.forEachObject(function(obj:any) {
            canvas.add(obj);
          });
          // this should solve the unselectability
          clonedObj.setCoords();
        } else {
          canvas.add(clonedObj);
        }
        canvas._clipboard.top += 15;
        canvas._clipboard.left += 15;
        canvas.setActiveObject(clonedObj);
        canvas.requestRenderAll();
      });
      e.ctrlKey=false
    }

  }
  else if(e.which === 65 && e.ctrlKey){
    e.preventDefault();
  
  }else
  if(e.which === 13 ){
    console.log(e.which);
    this.savedesign(e)
    e.preventDefault();
    e.which=false;

  }
  else
  if(e.which === 83 && e.ctrlKey){
   // console.log(e.which);
    key=e.which;
    this.savedesign(e)
    e.ctrlKey=false;
    e.preventDefault();

  }

});

}

onMouseMove(event:any){
  if(this.canvas.backgroundImage){
    if(this.canvas.getActiveObject()){
      this.Show()
    }
    var pointer = this.canvas.getPointer(event.e);
    var posX = pointer.x;
    var posY = pointer.y;
  if(posX>300||posX<300){
   /**
     if(this.canvas.getActiveObject()){
      this.canvas.getActiveObject().lockMovementX=true;
    }
    */ 
   

  }
 
  }
}

  }