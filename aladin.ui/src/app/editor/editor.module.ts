import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AladineditorComponent } from './aladineditor/aladineditor.component';
import { SharedModule } from '../shared';
import { ContextMenuModule } from 'ngx-contextmenu';
import { ColorSketchModule } from 'ngx-color/sketch';
import { ColorCircleModule } from 'ngx-color/circle'; 
import { ColorPhotoshopModule } from 'ngx-color/photoshop'; 
import { ColorMaterialModule } from 'ngx-color/material';
import { ColorSliderModule } from 'ngx-color/slider';
import { ColorShadeModule } from 'ngx-color/shade';

import { EditorRoutingModule } from './editor-routing.module';
import { DesignsComponent } from './designs/designs.component';
import { ProductsComponent } from './products/products.component';
import { DetailsComponent } from './details/details.component';
import { FacesComponent } from './faces/faces.component';
import { FormeComponent } from './forme/forme.component';
import { ClipartComponent } from './clipart/clipart.component';
import { TextComponent } from './text/text.component';
@NgModule({
  declarations: [
    AladineditorComponent,
    DesignsComponent,
    ProductsComponent,
    DetailsComponent,
    FacesComponent,
    FormeComponent,
    ClipartComponent,
    TextComponent,
  
  ],
  imports: [
    EditorRoutingModule,
    SharedModule,
    ContextMenuModule.forRoot(),
    ColorSketchModule,
    ColorCircleModule,
    ColorPhotoshopModule,
    ColorMaterialModule,
    ColorSliderModule,
    ColorShadeModule
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA ]
})
export class EditorModule { }
