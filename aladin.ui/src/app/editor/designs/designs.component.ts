import { Component, Input, OnInit,OnChanges,Output,EventEmitter} from '@angular/core';
import { LocalService } from 'src/app/core';
declare  var require:any;
var myalert=require('sweetalert2');
var $ = require("jquery");
@Component({
  selector: 'app-designs',
  templateUrl: './designs.component.html',
  styleUrls: ['./designs.component.scss']
})
export class DesignsComponent implements OnInit ,OnChanges{
@Input() url:any;
@Input() face1:any;
price:any=0;
unit_price=0;
mydesigns:any=[];
inpnmb =1;
prod:any=[];
active_prod:any;
hide_calco=true;
show_modal=true;
active=false;
size={
  m:false,
  l:false,
  xl:false,
  xxl:false
}
type:any;
shape:any;
@Output() newItemEvent =new EventEmitter<string>();

text="your're designs  !!!";

constructor(private localservice:LocalService) { }

 ngOnChanges(){
  this.mydesigns = this.localservice.items;
  if(this.mydesigns.length>0){
    this.prod=this.mydesigns;
  
  }
  if(this.face1!=undefined){
    this.toggle()
  }
  
 }
  ngOnInit(): void {

   $("body").children().first().before($(".modal"));

  }


  getPrice(){
    let price=this.unit_price
    this.inpnmb =+this.inpnmb+1;
      if(this.inpnmb>0&&price!=null){
        this.price= this.inpnmb*(+price);
      }
    }

  getPricem(){
    let price=this.unit_price
    this.inpnmb=this.inpnmb-1;
  if(this.inpnmb>0 && price!=null){
    if(((+this.inpnmb)*(+this.price))>0){
      this.price= ((+this.inpnmb)*(+price));
    }
  }else{
    this.inpnmb=this.inpnmb+1;

  }
    }

    decremet_by(){
      let price=this.unit_price
      this.inpnmb =(+this.inpnmb)*5;
      if(this.inpnmb>0&&price!=null){
        this.price= this.inpnmb*(+price);
      }

    }

    divide(){
      let price=this.unit_price
      if(this.inpnmb >5){
        this.inpnmb =~~((+this.inpnmb)/5);
        if(this.inpnmb> 0 && price!=null){
          this.price= this.inpnmb*(+price);
        }
      }
    }

    
  removeItem(id:any){
    if(id!=-1){
      this.localservice.removeOne(id);
    }
    
  }

  addtocart(event:any){
    let data = this.getId(this.active_prod);
    if(data!=-1){
      let cart_data={
       // in_stock:"En stock",
        qty:this.inpnmb,
        price:this.unit_price,
        t:this.price,
        id:data.prod.id,
        face1:data.face1,
        face2:data.face2,
        text1:data.text1,
        text2:data.text2,
        shape:this.shape,
        type:this.type,
        size:this.size,
        name:data.prod.name,
        production_f1:data.production_f1,
        production_f2:data.production_f2

       };

       this.localservice.adtocart(cart_data);
        myalert.fire({
          title:'<strong>produit ajouté</strong>',
          icon:'success',
          html:
            '<h6 style="color:blue">Felicitation</h6> ' +
            '<p style="color:green">Votre design a été ajouté dans le panier</p> ' +
            '<a href="/cart">Je consulte mon panier</a>' 
            ,
          showCloseButton: true,
          showCancelButton: true,
          focusConfirm: false,
          confirmButtonText:
            '<i class="fa fa-thumbs-up"></i> like!',
          confirmButtonAriaLabel: 'Thumbs up, great!',
          cancelButtonText:
            '<i class="fa fa-thumbs-down"></i>',
          cancelButtonAriaLabel:'Thumbs down'
        })
        this.removeItem(this.active_prod);
        if(this.localservice.items.length==0){
          location.reload()

        }

    }else{
      myalert.fire({
        title: "Erreur",
        text: "Une erreur s'est produite",
        icon: "error",
         button: "Ok"
        })
    }
  }

toggle(){
this.active=!this.active
}

hidemodal(){
  this.hide_calco=!this.hide_calco;
  console.log(this.size);
}


OnTypeChange(event:any){
  console.log(event.target.value);
  this.shape=event.target.value;
}

OnShapeChange(event:any){
  console.log(event.target.value);
  this.type=event.target.value;
}

getProdprice=(event:any)=>{
  console.log(event.target.name)
  this.unit_price=parseInt(event.target.name)
  this.price=this.unit_price
  this.inpnmb=1;
  this.active_prod=event.target.id;
  console.log(event.target.id);
  this.getId(this.active_prod);
}

getId=(id:any)=>{
  if(id!=-1){
    console.log(this.mydesigns[+id].prod.id)
    return this.mydesigns[+id];
  }else{
    return -1;
  }

}

cartlength(value:any){
  this.newItemEvent.emit(value);

}



}
