import { NgModule } from '@angular/core';
import { PanierComponent } from './panier/panier.component';
import { RouterModule, Routes } from '@angular/router';
const routes: Routes = [
  {path:'',component:PanierComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CartRoutingModule { }
