import { Component, OnInit,OnChanges } from '@angular/core';
import { LocalService } from 'src/app/core';
@Component({
  selector: 'app-panier',
  templateUrl: './panier.component.html',
  styleUrls: ['./panier.component.scss']
})
export class PanierComponent implements OnInit ,OnChanges{
 cart_items:any;
 sub_total=0;
 d=0;
 id:any;
 isempty=true;
 diliver=this.d;
 update=true;
 inpnmb=0;
 unit_price=1;
 price=0
  constructor(private item:LocalService) { }

  ngOnInit(): void {  
   this.cart_items= this.item.cart_items;
   if(this.cart_items.length>0){
    this.isempty=!this.isempty;
    for(let item of this.cart_items){
      this.sub_total=this.sub_total+(+item.t);
   }
   this.diliver= this.diliver+this.sub_total;
   }
   
  }


ngOnChanges(){
  this.cart_items= this.item.cart_items;
}



  getPrice(event:any){
    console.log(event.target.name);
    let data= this.getId(event.target.name)
     if(data!=-1) {
      let price=data.price 
      if(this.update){
        this.toggle()

        this.inpnmb =+data.qty+1;
        if(this.inpnmb>0&&price!=null){
          this.price= this.inpnmb*(+price);
          this.sub_total=this.price+this.sub_total
          console.log(this.inpnmb)
  
        }
      }else{

        console.log(event.target.name);
        this.inpnmb =+this.inpnmb+1;
        if(this.inpnmb>0&&price!=null){
          this.price= this.inpnmb*(+price);
          this.sub_total=this.price+this.sub_total
          console.log(this.inpnmb)

          
  
        }
      }
     } 
  
    }

  getPricem(event:any){
    console.log(event.target.name);

    let data= this.getId(event.target.name)
    let price=data.price 
    if(this.update){
      this.toggle()

      this.inpnmb=data.qty-1;
      if(this.inpnmb>0 && price!=null){
      if(((+this.inpnmb)*(+this.price))>0){
        this.price= this.inpnmb*(+price);
        this.sub_total=this.price+this.sub_total
        console.log(this.inpnmb)

      }
    }else{
      this.inpnmb=this.inpnmb+1;
  
    }

    }else{
      console.log(event.target.name);
      this.inpnmb=this.inpnmb-1;
      if(this.inpnmb>0 && price!=null){
      if(((+this.inpnmb)*(+this.price))>0){
        this.price= this.inpnmb*(+price);
        this.sub_total=this.price+this.sub_total
        console.log(this.inpnmb)

      }
    }else{
      this.inpnmb=this.inpnmb+1;
  
    }
    } 
   
    }

    decremet_by(event:any){
      console.log(event.target.name);

      let data= this.getId(event.target.name)
      let price=data.price   
      if(this.update){
        this.toggle();
        this.inpnmb =(data.qty)*5;
        if(this.inpnmb>0&&price!=null){
          this.price= this.inpnmb*(+price);
          this.sub_total=this.price+this.sub_total
          console.log(this.inpnmb)

        }
      }else{
        console.log(event.target.name);

        this.inpnmb =(this.inpnmb)*5;
        if(this.inpnmb>0&&price!=null){
          this.price= this.inpnmb*(+price);
          this.sub_total=this.price+this.sub_total
          console.log(this.inpnmb)
        }
      }
     
    }

    divide(event:any){
      console.log(event.target.name);

      let data= this.getId(event.target.name)
      let price=data.price    
      if(this.update){
        if(data.qty >5){
          this.toggle();
          this.inpnmb =~~((+data.qty)/5);
          if(this.inpnmb > 0 && price!=null){
            this.price= this.inpnmb*(+price);
            this.sub_total=this.price;

          }
        }
      }else{
        console.log(event.target.name);

        if(this.inpnmb >5){
          this.inpnmb =~~((+this.inpnmb)/5);
          if(this.inpnmb> 0 && price!=null){
            this.price = this.inpnmb*(+price);
            this.sub_total=this.price;

          }
        }

      }
 }

toggle()

{
this.update=!this.update
}

  removeItem(event:any){
    let id= event.target.id;
    let item;
     console.log(id)
    if(id!=-1){
      item=this.cart_items[+id]
      this.sub_total=this.sub_total - parseInt(item.t)
      this.item.removeItem(id);
    
    }
    
  }


  getId=(id:any)=>{
    if(id!=-1){
      return this.cart_items[+id];
    }else{
      return -1;
    }
  
  }
  
  finale_step(event:any){
    localStorage.setItem("Total",JSON.stringify(this.sub_total))
    location.href="/users/login"
  }


  

}
