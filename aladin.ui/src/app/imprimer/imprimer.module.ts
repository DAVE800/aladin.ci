import { SharedModule } from './../shared/shared.module';
import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ImprimerRoutingModule } from './imprimer-routing.module';
import { ImprimeComponent } from './imprime/imprime.component';


@NgModule({
  declarations: [
    ImprimeComponent
  ],
  imports: [
    CommonModule,
    ImprimerRoutingModule,
    SharedModule
  ],
  schemas:[
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class ImprimerModule { }
