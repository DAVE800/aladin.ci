import { Component, OnInit } from '@angular/core';
import { ListService } from 'src/app/core';
@Component({
  selector: 'app-listprint',
  templateUrl: './listprint.component.html',
  styleUrls: ['./listprint.component.scss']
})
export class ListprintComponent implements OnInit {
packs:any;
url="/editor/packs/";
  constructor(private l:ListService) { }

  ngOnInit(): void {
    this.l.getPacks().subscribe(res=>{this.packs=res},err=>{console.log(err)})
  }
  View(){
    let view = document.getElementById('view');
    view?.scrollIntoView({behavior:"smooth"})
   }
}
