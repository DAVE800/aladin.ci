import { Component, OnInit } from '@angular/core';
import { ListService } from 'src/app/core';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
 clothes:any
 url="/editor/cloth/"
  constructor(private cltd:ListService) { }

  ngOnInit(): void {
    this.cltd.getclothespage().subscribe(
      res=>{
        this.clothes = res.data
        console.log(this.clothes)
      },
      err=>{
        console.log(err)
      }
    )
  }
 
}
